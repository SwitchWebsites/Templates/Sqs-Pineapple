Now Websites - Pineapple Template
=======
>This is a modified template built from the Base Squarespace template. This is a custom template built by [Chris Limbrick](http://github.com/OneHunnid/pineapple). _This template is currently in development and being used for training purposes._ Erase this line when using for client. 
>  
>Source code and assets for https://now-websites-CLIENT.squarespace.com

## Template Family
>Pineapple - Custom

#### Features
- [x] Font Awesome v4.7.0
- [x] jQuery v3.1.1
- [x] Built-in tabs
- [x] Built-in accordion
- [x] Powered by Now Badge\*
- [ ] Smooth Page Transitions (AJAX)

_\*You can remove this by using a checkbox in the style editor_

## Template Hosting
#### Domains
Primary: https://now-websites-CLIENT.squarespace.com  
Built-In: https://now-websites-CLIENT.squarespace.com  

#### Git
Squarespace: https://now-websites-CLIENT.squarespace.com/template.git  
Gitlab: https://gitlab.com/NowStreamingServices/CLIENT.git  

## Author's Note

>Pineapple is a refreshing and more modular developer template for the Squarespace Developer Platform. The file structure for Pineapple is specifically meant to break out the code in more files for better organization through modularity.  
>1. The template's header, main content and footer are in their respective .region files. (e.g. header.region, site.region, footer.region)  
>2. Stylesheets are divided by global, header, footer, tweaks, typography, variables and reset.  
>3. Custom Index Collection that stacks pages is included.  
>4. Default Blog collection is included.  
